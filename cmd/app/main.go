package main

import (
	Container "windrise/internal/interfaces/container"
	. "windrise/internal/interfaces/server"
)

func main() {
	StartService(Container.New())
}
