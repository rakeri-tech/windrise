package entity

import "time"

type Users struct {
	ID        int64
	Username  string
	Email     string
	Password  string
	RoleId    int64
	CreatedAt time.Time
	UpdatedAt time.Time
	LastLogin time.Time
}

func (u *Users) TableName() string {
	return "users"
}
