package repository

import (
	"gitlab.com/doniapr-gopkg/pkg/session"

	"windrise/internal/domain/entity"
)

type Users interface {
	FindByUsername(session *session.Session, username string) (entity entity.Users, err error)
}
