package psql

import (
	"gitlab.com/doniapr-gopkg/pkg/session"
	"gorm.io/gorm"

	"windrise/internal/domain/entity"
	"windrise/internal/domain/repository"
)

type userRepository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) repository.Users {
	if db == nil {
		panic("database is nil")
	}

	return &userRepository{
		db: db,
	}
}

func (u *userRepository) FindByUsername(session *session.Session, username string) (entity entity.Users, err error) {
	err = u.db.Where("username= ?", username).First(&entity).Error
	return
}
