package container

import (
	"fmt"

	Logger "gitlab.com/doniapr-gopkg/logger"

	"windrise/internal/infrastructure/psql"
	Config "windrise/internal/shared/config"
	"windrise/internal/shared/database"
	"windrise/internal/usecase/users"
)

type Container struct {
	Config      *Config.DefaultConfig
	Logger      Logger.Logger
	UserService users.UserService
}

func New() *Container {
	fmt.Println("Try NewContainer ... ")

	// ========Construct Config
	config := Config.New("./resources/config.json")

	// ========Setup database
	db := database.NewDatabase(config.Database)

	// ========Setup Logger
	logger := Logger.SetupLoggerCombine(config.Logger)

	// ========Setup Repo
	userRepo := psql.NewRepository(db)
	userService := users.NewService(userRepo)

	container := &Container{
		Config:      config,
		Logger:      logger,
		UserService: userService,
	}

	return container
}
