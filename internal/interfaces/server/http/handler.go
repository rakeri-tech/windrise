package http

import "windrise/internal/interfaces/container"

type Handler struct {
	userHandler *userHandler
}

func SetUpHandler(container *container.Container) *Handler {
	userHandler := SetupUserHandler(container.UserService)
	return &Handler{
		userHandler: userHandler,
	}
}
