package http

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/doniapr-gopkg/pkg/vo"

	"windrise/internal/usecase/users"
)

type userHandler struct {
	userService users.UserService
}

func SetupUserHandler(userService users.UserService) *userHandler {
	if userService == nil {
		panic("userService is nil")
	}

	return &userHandler{
		userService: userService,
	}
}

func (handler *userHandler) FindUser(c echo.Context) error {
	ctx := vo.Parse(c)
	session := ctx.Session

	username := ctx.Param("username")
	resp, err := handler.userService.FindByUsername(session, username)
	if err != nil {
		return err
	}

	return ctx.Ok(resp)
}
