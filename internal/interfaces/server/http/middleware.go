package http

import (
	"bytes"
	"io/ioutil"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	session2 "gitlab.com/doniapr-gopkg/pkg/session"
	"gitlab.com/doniapr-gopkg/pkg/vo"

	"windrise/internal/interfaces/container"
	"windrise/internal/shared/utils"
)

func SetupMiddleware(server *echo.Echo, container *container.Container) {
	config := container.Config
	server.Use(func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			// Skip logging on healthcheck
			if c.Request().URL.Path == "/" {
				return h(c)
			}

			var bodyBytes []byte
			if c.Request().Body != nil {
				bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
			}
			// restore io.ReadCloser to its original state
			c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

			reqId := utils.GenerateThreadId()

			session := session2.New(container.Logger).
				SetThreadID(reqId).
				SetAppName(config.Apps.Name).
				SetAppVersion("0.0").
				SetPort(config.Apps.HttpPort).
				SetSrcIP(c.RealIP()).
				SetURL(c.Request().URL.String()).
				SetMethod(c.Request().Method).
				SetHeader(c.Request().Header).
				SetRequest(string(bodyBytes))
			//.SetContext

			session.T2("Incoming Request")

			c.Set(vo.AppSession, session)

			return h(c)
		}
	})

	server.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{echo.GET, echo.POST, echo.PUT, echo.PATCH, echo.DELETE, echo.HEAD, echo.OPTIONS},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))

	server.Validator = &DataValidator{ValidatorData: validator.New()}

}

type DataValidator struct {
	ValidatorData *validator.Validate
}

func (cv *DataValidator) Validate(i interface{}) error {
	return cv.ValidatorData.Struct(i)
}
