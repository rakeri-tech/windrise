package http

import (
	"github.com/labstack/echo/v4"

	"windrise/internal/interfaces/container"
)

func StartHttpService(container *container.Container) {
	server := echo.New()

	SetupMiddleware(server, container)
	SetUpRouter(server, SetUpHandler(container))

	if err := server.Start(container.Config.AppAddress()); err != nil {
		panic(err)
	}
}
