package server

import (
	"windrise/internal/interfaces/container"
	"windrise/internal/interfaces/server/http"
)

func StartService(container *container.Container) {
	http.StartHttpService(container)
}
