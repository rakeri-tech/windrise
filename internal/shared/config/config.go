package config

import (
	"encoding/json"
	"fmt"
	"gitlab.com/doniapr-gopkg/logger"
	"time"

	"github.com/spf13/viper"
)

type DefaultConfig struct {
	// TODO: add json tag ex "json:"name""
	Apps struct {
		Name     string `json:"name"`
		HttpPort int    `json:"httpPort"`
		GrpcPort int    `json:"grpcPort"`
	} `json:"apps"`
	Logger   logger.Options `json:"logger"`
	Database DatabaseConfig `json:"database"`
}

func New(path string) *DefaultConfig {
	fmt.Sprintf("Try new config...")

	viper.SetConfigFile(path)
	viper.SetConfigType("json")
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}
	defaultConfig := DefaultConfig{}
	err := viper.Unmarshal(&defaultConfig)
	if err != nil {
		panic(err)
	}

	b, _ := json.Marshal(defaultConfig)
	fmt.Println(b)

	return &defaultConfig
}

func (c *DefaultConfig) AppAddress() string {
	return fmt.Sprintf(":%v", c.Apps.HttpPort)
}

type DatabaseConfig struct {
	Username           string        `json:"username"`
	Password           string        `json:"password"`
	Name               string        `json:"name"`
	Schema             string        `json:"schema"`
	Host               string        `json:"host"`
	Port               int           `json:"port"`
	MinIdleConnections int           `json:"minIdleConnections"`
	MaxOpenConnections int           `json:"maxOpenConnections"`
	DebugMode          bool          `json:"debugMode"`
	SslMode            string        `json:"ssl"`
	ConnMaxLifetime    time.Duration `json:"connMaxLifetime"`
}
