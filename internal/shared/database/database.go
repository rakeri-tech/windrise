package database

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"windrise/internal/shared/config"
)

func NewDatabase(config config.DatabaseConfig) *gorm.DB {
	fmt.Println("Try Newdatabase...")
	dsn := fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s",
		config.Host, config.Port, config.Name, config.Username, config.Password, config.SslMode)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	sqlDB, err := db.DB()
	sqlDB.SetMaxIdleConns(config.MinIdleConnections)
	sqlDB.SetMaxOpenConns(config.MaxOpenConnections)

	if config.DebugMode {
		db.Debug()
	}

	return db
}
