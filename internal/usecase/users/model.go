package users

import "time"

type UsersResponse struct {
	Username  string
	Email     string
	Role      string
	LastLogin time.Time
}
