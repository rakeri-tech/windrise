package users

import "gitlab.com/doniapr-gopkg/pkg/session"

type UserService interface {
	FindByUsername(session *session.Session, username string) (resp UsersResponse, err error)
}
