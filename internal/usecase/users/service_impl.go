package users

import (
	"strconv"

	"gitlab.com/doniapr-gopkg/pkg/session"

	"windrise/internal/domain/repository"
)

type userService struct {
	userRepo repository.Users
}

func NewService(userRepo repository.Users) *userService {
	if userRepo == nil {
		panic("user Repo is nil")
	}

	return &userService{
		userRepo: userRepo,
	}
}

func (us *userService) FindByUsername(session *session.Session, username string) (resp UsersResponse, err error) {
	data, err := us.userRepo.FindByUsername(session, username)
	if err != nil {
		session.Error("Failed to get User Data", err.Error())
		return
	}
	if data.ID == 0 {
		session.Error("User Data not found", err.Error())
		return
	}

	resp = UsersResponse{
		Username:  data.Username,
		Email:     data.Email,
		Role:      strconv.Itoa(int(data.RoleId)),
		LastLogin: data.LastLogin,
	}
	return
}
