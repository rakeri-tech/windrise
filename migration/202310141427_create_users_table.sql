create table users (
                       id serial PRIMARY KEY ,
                       email varchar (255) unique not null,
                       username varchar(50) unique not null ,
                       password varchar (50) not null ,
                       role_id int not null ,
                       created_at timestamp not null,
                       updated_at timestamp,
                       last_login timestamp,
                       foreign key (role_id) references roles (id)
);
